import javax.swing.*;
import java.util.Map;

/**
 A simple invoice formatter.
 */
public class newFormat implements InvoiceFormatter
{
    public String formatHeader()
    {
        total = 0;
        String text = new String("     I N V O I C E\n\n\n");
        String htmlText = new String("<html><font color='red'>" + text + "</font></html>");
        JTextPane jTextPane =new JTextPane ();
        jTextPane.setContentType("text/html");
        jTextPane.setText(htmlText);
        return String.valueOf(jTextPane);
    }

    public String formatLineItem(LineItem item, Map<LineItem, Integer> itemIntegerMap)
    {

        total += item.getPrice() * itemIntegerMap.get(item);
        String countItems = item.toString() +  "      *      " + itemIntegerMap.get(item) + "     ";
        return (String.format(
                "%s: $%.2f\n",countItems,item.getPrice()));

    }

    public String formatFooter()
    {

        return (String.format("\n\nTOTAL DUE: $%.2f\n", total));
    }

    private double total;
}



