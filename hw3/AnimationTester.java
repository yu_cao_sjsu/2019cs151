import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
   This program implements an animation that moves
   a car shape.
*/
public class AnimationTester
{

   private static  int ICON_WIDTH = 400;
   private static  int ICON_HEIGHT = 400;
   private static  int CAR_WIDTH = 100;

   public static void main(String[] args)
   {
      JFrame frame = new JFrame();

      //
//      final int FIELD_WIDTH = 20;
//      final JTextField textField = new JTextField(FIELD_WIDTH);
//      textField.setText("Click a button!");

//      JButton largerButton = new JButton("Larger");
//
//      largerButton.addActionListener(event ->
//              // da
//              shape.setSize(1);
//               label.repaint();
//              );
//
//      JButton smallerButton = new JButton("Smaller");
//
//      smallerButton.addActionListener(event ->
//              //xiao
//               shape.setSize(-1);
//               label.repaint();
//              );
//

       MoveableShape shape
            = new CarShape(0, 0, CAR_WIDTH);

      ShapeIcon icon = new ShapeIcon(shape,
            ICON_WIDTH, ICON_HEIGHT);

      final JLabel label = new JLabel(icon);
      frame.setLayout(new FlowLayout());
    frame.add(label);
//
//      frame.add(largerButton);
//      frame.add(smallerButton);
      //frame.add(textField);
//

      frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      frame.pack();
      frame.setVisible(true);

      final int DELAY = 100;
      // Milliseconds between timer ticks
//      Timer t = new Timer(DELAY, event ->
//         {
//            shape.move();
//            label.repaint();
//         });
//      t.start();

      JButton largerButton = new JButton("Larger");

      largerButton.addActionListener(event ->
              // da
      {shape.setSize(1);
              label.repaint();}

              );

      JButton smallerButton = new JButton("Smaller");

      smallerButton.addActionListener(event ->
              //xiao
      {shape.setSize(-1);
              label.repaint();}
              );

      frame.add(largerButton);
      frame.add(smallerButton);

      label.repaint();
   }


}
