import java.util.Map;

/**
   A simple invoice formatter.
*/
public class SimpleFormatter implements InvoiceFormatter
{
   public String formatHeader()
   {
      total = 0;
      return "     I N V O I C E\n\n\n";
   }

   public String formatLineItem(LineItem item, Map<LineItem, Integer> itemIntegerMap)
   {

      total += item.getPrice() * itemIntegerMap.get(item);
      String countItems = item.toString() +  "      *      " + itemIntegerMap.get(item) + "     ";
      return (String.format(
            "%s: $%.2f\n",countItems,item.getPrice()));

   }

   public String formatFooter()
   {

      return (String.format("\n\nTOTAL DUE: $%.2f\n", total));
   }

   private double total;
}
