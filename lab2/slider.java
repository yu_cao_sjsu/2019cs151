import java.awt.*;
import java.util.*;
import javax.swing.*;
import javax.swing.event.*;

/**
 * @version 1.15 2015-06-12
 * @author Cay Horstmann
 */
public class slider
{
    public static void main(String[] args)
    {
        JFrame jFrame = new JFrame();
        // 创建画板
        JPanel jpanel = new JPanel() {

        }
            //序列号（可省略）
            private static final long serialVersionUID = 1L;

        SliderFrame frame = new SliderFrame();
        frame.setTitle("SliderTest");
        CarIcon me = new CarIcon(100);
        frame.add(me);
        //frame.add(, new CarIcon (100));
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
}

/**
 * A frame with many sliders and a text field to show slider values.
 */
class SliderFrame extends JFrame
{
    private JPanel sliderPanel;
    private JTextField textField;
    private ChangeListener listener;

    /**
     Gets a slider from this frame.
     @param i the index (0 = first slider)
     @return the i-th slider
     */
    public JSlider getSlider(int i)
    {
        return (JSlider) ((JPanel) sliderPanel.getComponent(i)).getComponent(0);
    }

    public SliderFrame()
    {
        sliderPanel = new JPanel();
        sliderPanel.setLayout(new GridBagLayout());

        // common listener for all sliders
        listener = event -> {
            // update text field when the slider value changes
            JSlider source = (JSlider) event.getSource();
            textField.setText("" + source.getValue());
        };

        // add a plain slider

        JSlider slider = new JSlider();


        slider = new JSlider();
        slider.setPaintTicks(true);
        slider.setPaintLabels(true);
        slider.setMajorTickSpacing(20);
        slider.setMinorTickSpacing(5);
        addSlider(slider, "Labels");

        // add a slider with alphabetic labels

        slider = new JSlider();
        slider.setPaintLabels(true);
        slider.setPaintTicks(true);
        slider.setMajorTickSpacing(20);
        slider.setMinorTickSpacing(5);


        // add a slider with icon labels

        slider = new JSlider();
        slider.setPaintTicks(true);
        slider.setPaintLabels(true);
        slider.setSnapToTicks(true);
        slider.setMajorTickSpacing(20);
        slider.setMinorTickSpacing(20);



        // add the text field that displays the slider value

        textField = new JTextField();
        add(sliderPanel, BorderLayout.CENTER);
        add(textField, BorderLayout.SOUTH);
        pack();
    }

    /**
     * Adds a slider to the slider panel and hooks up the listener
     * @param s the slider
     * @param description the slider description
     */
    public void addSlider(JSlider s, String description)
    {
        s.addChangeListener(listener);
        JPanel panel = new JPanel();
        panel.add(s);
        panel.add(new JLabel(description));
        panel.setAlignmentX(Component.LEFT_ALIGNMENT);
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridy = sliderPanel.getComponentCount();
        gbc.anchor = GridBagConstraints.WEST;
        sliderPanel.add(panel, gbc);
        //sliderPanel.add()
    }
}

