Your name: Yu Cao
Your student ID: 010716628
Your major:Applied Math
Your preferred email address: yu.cao@sjsu.edu 
Check one: [check ] Undergraduate, [ ] Graduate, [ ] Open university 
Repeating the class? [ check] Yes, [ ] No
Place and semester where you took a programming course in Java: CS 46A, CS46B, CS146
Course number and grade received: A; A; B
Place and semester where you took a course in data structures: CS146
Course number and grade received: B
Time it took you to answer the two questions below: 1 hour
Did you ask anyone for help? [ ] Yes, [ check ] No