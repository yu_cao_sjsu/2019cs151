public class Strings
{
   public static String uniqueLetters(String str)
   {	
   	String res = "";
      for (int i = 0; i < str.length(); i++) {
      	String me = str.replace(str.substring(i, i+1), "");
      	if (me.length() == str.length()-1)
      	   res += str.substring(i, i+1);
      }
      return res;
   }
}