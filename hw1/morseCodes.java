import java.util.*;

public class Encoding
{
   public static Set<String> morseCodes(int m, int n)
   {
      Set<String> res = new TreeSet<>();
      // m dots
      // n dash
      StringBuilder path = new StringBuilder();
      dfshelper(res, path, m - 1, n, '.');
      dfshelper(res, path, m, n - 1, '-');
      return res;
   }
   public static void dfshelper(Set<String> res, StringBuilder path,
   int m, int n, char c){
      // op at node
      path.append(c);
      if(m == 0 && n == 0){
         res.add(path.toString());
      }
      // go down
      if(m > 0){
         dfshelper(res, path, m - 1, n, '.');
      }
      if(n > 0){
         dfshelper(res, path, m, n - 1, '-');
      }
      // go up
      path.deleteCharAt(path.length() - 1);
   }
}