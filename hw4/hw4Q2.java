import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 This program implements an animation that moves
 a car shape.
 */
public class hw4Q2
{
    public static void main(String[] args)
    {
        JFrame frame = new JFrame();

        final MoveableShape shape1
                = new CarShape(0, 0, CAR_WIDTH);
        final MoveableShape shape2
                = new CarShape(0, 0, CAR_WIDTH);

        ShapeIcon icon1 = new ShapeIcon(shape1,
                ICON_WIDTH, ICON_HEIGHT);
        ShapeIcon icon2 = new ShapeIcon(shape2,
                ICON_WIDTH, ICON_HEIGHT);

        final JLabel label1 = new JLabel(icon1);
//        frame.setLayout(new FlowLayout());
        frame.add(label1);
        final JLabel label2 = new JLabel(icon2);
        frame.setLayout(new FlowLayout());
        frame.add(label2);

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        int Delay1 = 100;
        int Delay2 = 500;

        Runnable r1 = new AnimationProducer(Delay1, label1, shape1);
        Runnable r2 = new AnimationProducer(Delay2, label2, shape2);

        Thread t1 = new Thread(r1);
        Thread t2 = new Thread(r2);

        t1.start();
        t2.start();
    }

    private static final int ICON_WIDTH = 400;
    private static final int ICON_HEIGHT = 100;
    private static final int CAR_WIDTH = 100;
}
