public class hw4Q1PartA {
    public static void main(String[] args) {

        System.out.println("10 tries are supposed to produce same balance.");
        for (int j = 0; j < 10; j ++) {
            Account account = new Account("CorruptedAccount", 100);
            System.out.println("This is " + j + "th Try");
            final int NUM = 100;

            Thread[] threads = new Thread[NUM];
            for (int i = 0; i < NUM; i ++) {
                if (threads[i] == null) {
                    threads[i] = new Thread(() -> {
                        account.increaseAmt(100);
                        account.decreaseAmt(100);
                    });
                    threads[i].start();
                }
            }

            for (int i = 0; i < NUM; i++) {
                try {
                    threads[i].join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            account.printMsg();
        }

        System.out.println("bank account can become corrupted, race condition occurred");

    }
}
