import java.util.concurrent.TimeUnit;

public class SyncAccount {

    private String name;
    private int balance;
        public SyncAccount(String _name, int _balance) {
            name = _name;
            balance = _balance;
        }

        public  void  increaseAmt(float increaseAmt){

            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            synchronized (this) {
                balance+=increaseAmt;
            }
        }

        public  void decreaseAmt(float decreaseAmt){
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            synchronized (this) {
                balance-=decreaseAmt;
            }

        }

        public void printMsg(){
            System.out.println(name+"balance："+balance);
        }



}
