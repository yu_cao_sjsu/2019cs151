import javax.swing.*;

public class AnimationProducer implements Runnable {
    private int Delay;
    private JLabel label;
    private MoveableShape shape;
    public AnimationProducer(int _Delay, JLabel _label, MoveableShape _shape){
        Delay = _Delay;
        label = _label;
        shape = _shape;
    }

    public void run()
    {
            Timer t = new Timer(Delay, event ->
            {
                shape.move();
                label.repaint();
            });
            t.start();

    }


}
