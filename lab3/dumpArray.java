
//Write a method dumpArray that prints the elements of any array to System.out, using toString on the array elements if the array elements are objects.




public class dumpArray
{

    public static void dumpArray(Object[] a)
    {

            Class c = a.getClass();
                if(c.getComponentType() instanceof Object){
                    System.out.println("toString() used : ");
                    for(int i = 0; i < a.length; i++)
                    System.out.println(a[i].toString());}
                else{
                    for(int i = 0; i < a.length; i++)
                    System.out.println(a[i]);
                    }


    }

    public static void main(String[] args)
    {
//        int[] ints = new int[]{1,2,3,4};
//        dumpArray((Object)ints);


        myObject m1 = new myObject(1, "obj");
        myObject m2 = new myObject(2, "obj");
        myObject m3 = new myObject(3, "obj");
        myObject m4 = new myObject(4, "obj");


        myObject[] objects = new myObject[4];
        objects[0] = m1;
        objects[1] = m2;
        objects[2] = m3;
        objects[3] = m4;
        dumpArray(objects);
    }

    public static class myObject{
        public int myInt;
        public String myString;

        public myObject(int i, String s){
            myInt = i;
            myString = s;
        }

        public String toString(){
            return myString + myInt;
        }

    }

}
